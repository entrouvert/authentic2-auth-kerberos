@Library('eo-jenkins-lib@main') import eo.Utils

pipeline {
    agent any
    options {
       disableConcurrentBuilds()
       skipDefaultCheckout()
    }
    environment {
       TMPDIR = "$WORKSPACE"
    }
    stages {
        stage('Unit Tests') {
            steps {
                cleanWs()
                dir ("repository") {
                    checkout scm
                    sh """
python3 -m venv ${env.TMPDIR}/venv/
${env.TMPDIR}/venv/bin/pip install tox
PGPORT=`python3 -c 'import struct; import socket; s=socket.socket(); s.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER, struct.pack("ii", 1, 0)); s.bind(("", 0)); print(s.getsockname()[1]); s.close()'` pg_virtualenv -o fsync=off ${env.TMPDIR}/venv/bin/tox -r"""
                }
            }
            post {
                always {
                    dir("repository") {
                        script {
                            utils = new Utils()
                            utils.publish_coverage('coverage.xml')
                            utils.publish_coverage_native('index.html')
                            utils.publish_pylint('pylint.out')
                        }
                        mergeJunitResults()
                    }
                }
            }
        }
        stage('Packaging') {
            steps {
                script {
                    env.SHORT_JOB_NAME=sh(
                        returnStdout: true,
                        // given JOB_NAME=gitea/project/PR-46, returns project
                        // given JOB_NAME=project/main, returns project
                        script: '''
                            echo "${JOB_NAME}" | sed "s/gitea\\///" | awk -F/ '{print $1}'
                        '''
                    ).trim()
                    if (env.GIT_BRANCH == 'main' || env.GIT_BRANCH == 'origin/main') {
                        sh "sudo -H -u eobuilder /usr/local/bin/eobuilder -d bullseye ${SHORT_JOB_NAME}"
                    } else if (env.GIT_BRANCH.startsWith('hotfix/')) {
                        sh "sudo -H -u eobuilder /usr/local/bin/eobuilder -d bullseye --branch ${env.GIT_BRANCH} --hotfix ${SHORT_JOB_NAME}"
                    }
                }
            }
        }
    }
    post {
        always {
            dir("repository") {
                script {
                    utils = new Utils()
                    utils.mail_notify(currentBuild, env, 'ci+jenkins-authentic2-auth-kerberos@entrouvert.org')
                }
            }
        }
    }
}
