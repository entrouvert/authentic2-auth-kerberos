# authentic2-auth-kerberos - Kerberos plugin for authentic2
# Copyright (C) 2010-2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from authentic2.custom_user.models import User


@pytest.fixture(autouse=True)
def kerberos(monkeypatch):
    import django_kerberos.views

    class MockKerberos(object):
        server_init_result = 1
        server_step_result = 1
        server_response_result = 'x'
        principal = 'user@REALM'

        def set(self, **kwargs):
            self.__dict__ = kwargs

        def authGSSServerInit(self, service):
            return self.server_init_result, {}

        def authGSSServerStep(self, context, authstr):
            return self.server_step_result

        def authGSSServerResponse(self, context):
            return self.server_response_result

        def authGSSServerUserName(self, context):
            return self.principal

        def authGSSServerClean(self, context):
            pass

        class KrbError(Exception):
            pass
    monkeypatch.setattr('django_kerberos.views.kerberos', MockKerberos())
    return django_kerberos.views.kerberos


def test_default(settings, app, db):
    settings.A2_AUTH_KERBEROS_DJANGO_BACKEND = True

    assert User.objects.count() == 0
    assert 'a2_kerberos_ok' not in app.cookies
    response = app.get('/login/')
    assert 'login-kerberos' in response.text
    assert 'autologin' not in response.text
    response = response.forms['kerberos-form'].submit(name='login-kerberos')
    assert response.location == '/accounts/kerberos/login/'
    response = response.follow(headers={'Authorization': 'Negotiate y'})
    assert app.cookies['a2_kerberos_ok'] == '1'
    assert response.location == '/'
    assert User.objects.count() == 1
    assert User.objects.get(username='user@realm')

    # logout
    app.session.flush()
    response = app.get('/login/')
    assert 'login-kerberos' in response.text
    assert 'autologin' in response.text
