# -*- coding: utf-8 -*-
# authentic2-auth-kerberos - Kerberos plugin for authentic2
# Copyright (C) 2010-2019 Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pytest
import django_webtest

from authentic2.custom_user.models import User
from authentic2.a2_rbac.utils import get_default_ou


@pytest.fixture
def app_factory(settings, tmpdir):
    settings.MEDIA_ROOT = str(tmpdir.mkdir('media'))
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    try:
        def factory(hostname='localhost'):
            return django_webtest.DjangoTestApp(extra_environ={'HTTP_HOST': hostname})
        yield factory
    finally:
        wtm._unpatch_settings()


@pytest.fixture
def app(app_factory):
    return app_factory()


@pytest.fixture
def create_user():
    def func(**kwargs):
        password = kwargs.pop('password', None) or kwargs['username']
        user, created = User.objects.get_or_create(**kwargs)
        if password:
            user.set_password(password)
            user.save()
        return user
    return func
