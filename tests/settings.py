import os
import authentic2_auth_kerberos


LANGUAGE_CODE = 'en'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'TEST': {
            'NAME': 'authentic2-auth-kerberos',
        },
    }
}

if 'postgres' in DATABASES['default']['ENGINE']:
    for key in ('PGPORT', 'PGHOST', 'PGUSER', 'PGPASSWORD'):
        if key in os.environ:
            DATABASES['default'][key[2:]] = os.environ[key]

ALLOWED_HOSTS = ['localhost']
A2_AUTH_KERBEROS_ENABLE = True

TENANT_APPS = []

INSTALLED_APPS, TENANT_APPS, AUTHENTICATION_BACKENDS, AUTH_FRONTENDS = authentic2_auth_kerberos.register(
    INSTALLED_APPS, TENANT_APPS, AUTHENTICATION_BACKENDS, AUTH_FRONTENDS
)
