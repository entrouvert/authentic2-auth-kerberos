from django.apps import AppConfig


class Authentic2AuthKerberosConfig(AppConfig):
    name = 'authentic2_auth_kerberos'
    label = 'authentic2_auth_kerberos'

    def ready(self):
        # patch authentic2.backends.ldap_backend.LDAPBackend for keys specific to Kerberos support
        from . import backends

    def get_a2_plugin(self):
        return Plugin()


class Plugin(object):
    def get_before_urls(self):
        from . import urls
        return urls.urlpatterns
