from django.utils.translation import gettext_noop
from django.shortcuts import render

from authentic2 import authenticators
from authentic2.utils.misc import redirect_to_login

from . import app_settings


class KerberosAuthenticator(authenticators.BaseAuthenticator):
    def enabled(self):
        return app_settings.ENABLE

    def name(self):
        return gettext_noop('Kerberos')

    def id(self):
        return 'kerberos'

    def login(self, request, *args, **kwargs):
        context = kwargs.get('context', {})
        # autologin is allowed if :
        # * a Kerberos login has already happened on this terminal
        # * we do not come from the logout view
        if (request.COOKIES.get('a2_kerberos_ok') == '1'
                and 'a2_just_logged_out' not in request.COOKIES):
            context['autologin'] = True
        submit = request.method == 'POST' and 'login-kerberos' in request.POST
        if submit:
            return redirect_to_login(request, login_url='kerberos-login')
        return render(request, 'authentic2_auth_kerberos/login.html', context)
