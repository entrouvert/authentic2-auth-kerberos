default_app_config = 'authentic2_auth_kerberos.apps.Authentic2AuthKerberosConfig'


def register(installed_apps, tenant_apps, authentication_backends, auth_frontends):
    apps = []
    for app in ('authentic2_auth_kerberos', 'django_kerberos'):
        if app not in installed_apps:
            apps.append(app)
    if apps:
        installed_apps += tuple(apps)
        tenant_apps += tuple(apps)

    for backend in [
            'authentic2_auth_kerberos.backends.A2LdapKerberosBackend',
            'authentic2_auth_kerberos.backends.A2KerberosBackend']:
        if backend not in authentication_backends:
            authentication_backends += (backend,)

    if 'authentic2_auth_kerberos.authenticators.KerberosAuthenticator' not in auth_frontends:
        auth_frontends += ('authentic2_auth_kerberos.authenticators.KerberosAuthenticator',)

    return installed_apps, tenant_apps, authentication_backends, auth_frontends
