import logging

from django_kerberos.views import NegotiateView

from authentic2.utils.misc import login as a2_utils_misc_login

__ALL_ = ['login']


class A2NegotiateView(NegotiateView):
    authentication_successful = False

    def __init__(self, *args, **kwargs):
        super(A2NegotiateView, self).__init__(*args, **kwargs)
        self.logger = logging.getLogger(__name__)

    def login_user(self, request, user):
        self.authentication_successful = True
        a2_utils_misc_login(request, user, 'kerberos')

    def principal_valid(self, request, *args, **kwargs):
        response = super(A2NegotiateView, self).principal_valid(request, *args, **kwargs)
        if self.authentication_successful:
            # set cookie so that automatic login will be tried next time
            response.set_cookie('a2_kerberos_ok', '1', max_age=86400 * 365)
        return response


login = A2NegotiateView.as_view()
