from django.conf.urls import url

from . import views

urlpatterns = [
        url(r'^accounts/kerberos/login/$', views.login, name='kerberos-login'),
]
